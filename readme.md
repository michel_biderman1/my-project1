###########  Création du site de petites annonces   ###########

1_ make:controller MainController
2_ php bin/console doctrine:database:create après configuration du fichier .env
3_ php bin/console make:user Users
4_ php bin/console make:migration + php bin/console doctrine:migrations:migrate
5_ php bin/console make:auth -> pour se logger
6_ symfony console make:registration-form
 Et refaire une migration 
7_ mdp oublié 
    insatallation du bundle nécessaire : composer require symfonycasts/reset-password-bundle
    => php bin/console make:reset-password 


########################     Installation de ckeditor et configuration    ########################
Installation:
1_ composer require friendsofsymfony/ckeditor-bundle
2_ symfony console ckeditor:install
3_ symfony console assets:install public

Configuration:


Créer la page profil
1_ Décommenter la ligne user dans security.yaml

#########################   Test de modification d'une annonce    #########################

Dans UsersController:
    => faire une fonction qui prend l'id de l'annonce 
        + qui récupère les nouvelles données 
        + enregistrement en db

Solution : copy/paste fonction ajout avec id en paramètre de la route + injection de l'annonce

====> Test ok


#################           Esapce Admin         ##################
1_ Décommenter la ligne admin dans security.yaml


###############             recherche fulltext          ################

