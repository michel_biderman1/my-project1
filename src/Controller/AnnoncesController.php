<?php

namespace App\Controller;

use App\Entity\Annonces;
use App\Form\AnnonceContactType;
use App\Repository\AnnoncesRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/annonces", name="annonces_")
 * @package App\Controller
 */
class AnnoncesController extends AbstractController
{
    /**
     * @Route("/details/{slug}", name="details")
     */
    public function detail($slug, AnnoncesRepository $annoncesRepo, Request $request, MailerInterface $mailer ): Response
    {
        $annonce = $annoncesRepo->findOneBy(['slug' => $slug]);

        if(!$annonce){
            throw new NotFoundHttpException('Aucune annonce n\'a été trouvée');
        }

        $form = $this->createForm(AnnonceContactType::class);

        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $email = (new TemplatedEmail())
                        ->from($contact->get('email')->getData())
                        ->to(new Address($annonce->getUsers()->getEmail()))
                        ->subject('Contact au sujet de votre annonce "' . $annonce->getTitle() . '"')
                        ->htmlTemplate('email/contact_annonce.html.twig')
                        ->context([
                            'annonce' => $annonce,
                            'mail' => $contact->get('email')->getData(),
                            'message' => $contact->get('message')->getData()
                        ]);
                    
            $mailer->send($email);

            $this->addFlash('email', 'Votre email a bien été envoyé');

            return $this->redirectToRoute('annonces_details', ['slug' => $annonce->getSlug()]);
        }

        return $this->render('annonces/details.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/favoris/ajout/{id}", name="ajout_favoris")
     */
    public function ajoutFavoris(Annonces $annonce): Response
    {
        
        if(!$annonce){
            throw new NotFoundHttpException('Aucune annonce n\'a été trouvée');
        }

        $annonce->addFavori($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($annonce);
        $em->flush();

        return $this->redirectToRoute('annonces_show');
    }

    /**
     * @Route("/favoris/enlever/{id}", name="enlever_favoris")
     */
    public function enleverFavoris(Annonces $annonce): Response
    {
        
        if(!$annonce){
            throw new NotFoundHttpException('Aucune annonce n\'a été trouvée');
        }

        $annonce->removeFavori($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($annonce);
        $em->flush();

        return $this->redirectToRoute('annonces_show');
    }

}
