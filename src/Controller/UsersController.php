<?php

namespace App\Controller;

use App\Entity\Annonces;
use App\Entity\Images;
use App\Form\AnnoncesType;
use App\Form\EditProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index(): Response
    {
        return $this->render('users/index.html.twig');
    }

    /**
     * @Route("/users/annonces/ajout", name="users_annonces_ajout")
     */
    public function ajoutAnnonce(Request $request){

        $annonce = new Annonces();

        $form = $this->createForm(AnnoncesType::class, $annonce);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $annonce->setUsers($this->getUser());
            $annonce->setActive(false);

            // On récupère les images transmises
            $images = $form->get('images')->getData();

            // On boucle sur les images
            foreach($images as $image){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                //On copie le cfichier dans le dossier upload
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On stocke l'image en bdd (son nom)
                $img = new Images();
                $img->setName($fichier);
                $annonce->addImage($img);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($annonce);
            $em->flush();

            $this->addFlash('messageAjout', 'L\'annonce '.$annonce->getTitle().' a été ajoutée avec succès !');
            
            return $this->redirectToRoute('users_mes_annonces');

        }

        return $this->render('users/annonces/edit.html.twig',[
            'form' => $form->createView(),
            'titre' => 'Ajouter une annonce:'
        ]);
    }

    /**
     * @Route("/users/annonces/edit/{id}", name="users_annonces_edit")
     */
    public function editAnnonce(Request $request, Annonces $annonce){


        $form = $this->createForm(AnnoncesType::class, $annonce);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
             // On récupère les images transmises
             $images = $form->get('images')->getData();

            // On boucle sur les images
            foreach($images as $image){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier upload
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On stocke l'image en bdd
                $img = new Images();
                $img->setName($fichier);
                $annonce->addImage($img);
            }

            $annonce->setUsers($this->getUser());
            // $annonce->setActive(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($annonce);
            $em->flush();

            $this->addFlash('messageEdit', 'L\'annonce '.$annonce->getTitle().' a été mise à jour !');
            
            return $this->redirectToRoute('users_mes_annonces');

        }

        return $this->render('users/annonces/edit.html.twig',[
            'form' => $form->createView(),
            'annonce' => $annonce,
            'titre' => 'Modifier cette annonce:'
        ]);
    }


    /**
     * @Route("/users/annonces", name="users_mes_annonces")
     */
    public function mesAnnonces(){
        $user = $this->getUser();
        $annonces = $user->getAnnonces();

        return $this->render('users/annonces/mes_annonces.html.twig', [
            'annonces' => $annonces
        ]);
    }

    /**
     * @Route("/users/annonces/favoris", name="users_mes_favoris")
     */
    public function favoritesAnnonces(){
        $user = $this->getUser();
        $annoncesFavoris = $user->getFavoris();

        return $this->render('users/annonces/favoris.html.twig', [
            'favoritesAnnonces' => $annoncesFavoris
        ]);
    }

    /**
     * @Route("/users/edit", name="users_edit")
     */
    public function editProfile(Request $request){

        $user = $this->getUser();

        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Votre profil a été mis à jour avec succès !');
            
            return $this->redirectToRoute('users');

        }

        return $this->render('users/edit_profile.html.twig',[
            'form' => $form->createView(),
        ]);
        
    }

    /**
     * @Route("/users/pass/edit", name="users_pass_edit")
     */
    public function editPass(Request $request, UserPasswordEncoderInterface $passwordEncoder){

        if($request->isMethod('POST')){
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            // On vérifie si les mots de passe sont identiques
            if($request->request->get('pass') == $request->request->get('pass2')){
                $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('pass2')));
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('message', 'Mot de passe mis à jour avec succès');

                return $this->redirectToRoute('users');
            }else{
                $this->addFlash('errorMessage', 'Les deux mots de passe ne sont pas identiques !');
            } 
        }

        return $this->render('users/edit_pass.html.twig');
    }

     /**
     * @Route("users/annonces/supprimer/{id}", name="users_annonce_supprimer")
     */
    public function deleteAnnonce(Request $request, Annonces $annonce): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($annonce);
        $em->flush();

        $this->addFlash('messageAnnonce', 'Annonce supprimée avec succès !');

        return $this->redirectToRoute('users_mes_annonces');
    }


    /**
     * @Route("/users/annonces/delete/image/{id}", name="annonces_delete_image", methods={"DELETE"})
     */
    public function deleteImage(Images $image, Request $request){
        $data = json_decode($request->getContent(), true);

        // On vérifie si le token est valide
        if($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])){
            // On récupère le nom de l'image
            $nom = $image->getName();
            // On supprime le fichier
            unlink($this->getParameter('images_directory') . '/' . $nom);

            // On supprime le chemin en db
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        }else{
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }

}
