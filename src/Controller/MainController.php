<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Form\SearchAnnoncesType;
use App\Repository\AnnoncesRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request, AnnoncesRepository $annoncesRepo): Response
    {
        $annonces = $annoncesRepo->findBy(['active' => true], ['createdAt' => 'desc']);

        $form = $this->createForm(SearchAnnoncesType::class);

        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On recherche les annonces correspondantes aux mots clés
            $annonces = $annoncesRepo->search(
                $search->get('mots')->getData(),
                $search->get('categorie')->getData()
            );
        }

        return $this->render('main/index.html.twig', [
            'annonces' => $annonces,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/annonces/show", name="annonces_show")
     */
    public function showAnnconces(AnnoncesRepository $annoncesRepo, Request $request): Response
    {
        $annonces = $annoncesRepo->findBy(['active' => true]);
        
        if($request->isMethod('POST')){
            $search = $request->request->get('recherche');
            if($search == ''){
                $this->redirectToRoute('');
            }
            if($search != null){
                $annonces = $annoncesRepo->search($search);
            }
            
        }
        return $this->render('annonces/show_annonces.html.twig', [
            'annonces' => $annonces
        ]);                 
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);

        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $email = (new TemplatedEmail())
                    ->from($contact->get('email')->getData())
                    ->to(new Address('moi@mail.fr'))
                    ->subject('Contact depuis le site "Petites-annonces": ' . $contact->get('sujet')->getData())
                    ->htmlTemplate('email/contact.html.twig')
                    ->context([
                        'mail' => $contact->get('email')->getData(),
                        'sujet' => $contact->get('sujet')->getData(),
                        'message' => $contact->get('message')->getData()
                    ]);
            $mailer->send($email);

            $this->addFlash('email', 'Votre email a bien été envoyé');

            return $this->redirectToRoute('contact');
        }
        
        return $this->render('main/contact.html.twig', [
            'form' =>$form->createView()
        ]);
    }

}
